# https://leetcode.com/problems/pascals-triangle/submissions/
# https://takeuforward.org/data-structure/program-to-generate-pascals-triangle/
from typing import List

class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        matrix = []
        for i in range(0,numRows):
            # print(i)
            a =[]
            for j in range(0, i+1):
                if (j == 0 or j == i):
                    a.append(1)
                else:
                    a.append(matrix[i-1][j-1] + matrix[i-1][j])
            matrix.append(a)
        return matrix
solution = Solution()
n = int(input("Please enter number of rows in pascal triangle"))
print(solution.generate(n))