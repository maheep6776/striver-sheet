# https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
# prices = [7,1,5,3,6,4]
# prices = [7,6,4,3,1]
prices = [1,2]
# prices = [3,2,6,5,0,3]
n = len(prices)
max = 0
for i in range (0,n):
    for j in range(i+1, n):
        if(max < (prices[j]-prices[i])):
            max = (prices[j]-prices[i])
print(max)

