# code url on leetcode https://leetcode.com/problems/set-matrix-zeroes/
# https://takeuforward.org/data-structure/set-matrix-zero/
from typing import List
# matrix = [[1,1,1],[1,0,1],[1,1,1]]
# matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
# matrix = [[1,2,3,4],[5,0,7,8],[0,10,11,12],[13,14,15,0]]
class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        zerosPositionForRow = []
        zerosPositionForColumn = []
        for index, each in enumerate(matrix):
            for innerIndex, innerEach in enumerate(matrix[index]):
                if(innerEach == 0):
                    zerosPositionForRow.append(index)
                    zerosPositionForColumn.append(innerIndex)
        # print(zerosPositionForRow)
        # print(zerosPositionForColumn)
        for index, each in enumerate(matrix):
            for innerIndex, innerEach in enumerate(matrix[index]):
                if(index in zerosPositionForRow or innerIndex in zerosPositionForColumn):
                    matrix[index][innerIndex] = 0
        # print(matrix)
        return matrix

solution = Solution() 
print("Enter input rows")
n = int(input())
print("Enter Ouput Rows")
m = int(input())
matrix = []
print("enter element row wise")
for i in range (0, n):
    a =[]
    for j in range(0, m):
        a.append(int(input()))
    matrix.append(a)
    
print(solution.setZeroes(matrix=matrix))
# matrix = [[1,1,1],[1,0,1],[1,1,1]]



